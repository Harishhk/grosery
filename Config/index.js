module.exports= {
    notFound : {
        apiStatus : false,
        statusCode : 404,
        result: {},
        message : 'Url Not Found'
    },
    mandatoryError  : {
        apiStatus: false,
        statusCode : 400,
        result: {},
        message: "Mandatory field is missing!"
    },
    somethingWentWrong : {
        apiStatus: false,
        statusCode : 400,
        result: {},
        message: "Something went wrong!"
    },
    noAccess : {
        statusCode: 401, 
        apiStatus: false, 
        result: {}, 
        message: "You don't have access!"
    },
    imgBaseUrl : 'http://18.224.93.136:5526/'
}