const {DataTypes} = require('sequelize');
module.exports = (sql) => {
    let Duration = sql.define('duration', {
        name: {
            type: DataTypes.STRING
        },
        startTime : {
            type: String
        },
        endTime : {
            type : String
        },
        activeStatus :  {
            type : DataTypes.NUMBER
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return Duration;
}