// Order
let {DataTypes} = require('sequelize');
module.exports = (sql) => {
    let Order = sql.define('orders', {
        userId : {
            type : DataTypes.NUMBER
        },
        durationId : {
            type : DataTypes.NUMBER
        },
        voucher : {
            type : DataTypes.NUMBER
        },
        secret: {
            type : DataTypes.STRING
        },
        offerAmount : {
            type : DataTypes.NUMBER // Only if voucher added
        },
        total : {
            type : DataTypes.NUMBER
        },
        status : {
            type : DataTypes.NUMBER // 0 for Pending, 1 for assigned, 2 for cancelled by user , 3 for compleated, 4 for Staff Decline
        },
        address  : {
            type : DataTypes.NUMBER
        },
        deliveryType : {
            type : DataTypes.NUMBER // 1 for online, 2 COD
        },
        contactId: {
            type : DataTypes.NUMBER
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return Order;
}