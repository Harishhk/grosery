// SubCategoryType
const {DataTypes} = require('sequelize');
module.exports = (sql) => {
    let SubCategoryType = sql.define('subcategorytype',{
        name : {
            type: DataTypes.STRING
        },
        subCategoryId : {
            type : DataTypes.NUMBER
        },
        activeStatus : {
            type: DataTypes.NUMBER // activeStatus 1 for active, 2 for in active
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return SubCategoryType;
}