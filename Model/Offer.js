// Offer
let {DataTypes} = require('sequelize');
module.exports = (sql) => {
    let Offer = sql.define('offer', {
        name : {
            type : DataTypes.STRING
        },
        percentage  : {
            type : DataTypes.STRING
        },
        description: {
            type : DataTypes.TEXT
        },
        image: {
            type : DataTypes.STRING
        },
        offerCode : {
            type : DataTypes.STRING
        },
        expiresdate: {
            type: DataTypes.DATE
        },
        activeState : {
            type : DataTypes.NUMBER // 1 for active, 2 for inActive
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return Offer;
}