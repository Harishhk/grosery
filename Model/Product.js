let {DataTypes} = require('sequelize');
module.exports = (sql) => {
    let Product = sql.define('product', {
        name :{
            type : DataTypes.STRING
        },
        price :{
            type : DataTypes.NUMBER
        },
        stock :{
            type : DataTypes.NUMBER
        },
        description : {
            type : DataTypes.TEXT
        },
        image : {
            type: DataTypes.STRING
        },
        units : {
            type : DataTypes.STRING
        },
        salesPrice : {
            type : DataTypes.NUMBER
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return Product;
}