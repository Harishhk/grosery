let {DataTypes} = require('sequelize');

module.exports = (sql) => {
    let Admin = sql.define('admin',{
        name : {
            type : DataTypes.STRING
        },
        email : {
            type : DataTypes.STRING,
            allowNull: false
        },
        password : {
            type : DataTypes.STRING
        },
        deleteStatus : {
            type : DataTypes.NUMBER
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return Admin;
}