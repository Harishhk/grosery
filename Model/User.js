let {DataTypes} = require('sequelize');
module.exports = (sql) => {
    let User = sql.define('user', {
        firstName : {
            type : DataTypes.STRING,
            allowNull: false
        },
        lastName : {
            type : DataTypes.STRING
        },
        email : {
            type : DataTypes.STRING,
            allowNull: false
        },
        phone : {
            type : DataTypes.NUMBER
        },
        deviceToken : {
            type : DataTypes.STRING
        },
        password : {
            type : DataTypes.STRING
        },
        deleteStatus : {
            type : DataTypes.NUMBER
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return User
}