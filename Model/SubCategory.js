// SubCategory
const {DataTypes} = require('sequelize');
module.exports = (sql) => {
    let SubCategory = sql.define('subcategory',{
        name : {
            type : DataTypes.STRING
        },
        icon : {
            type : DataTypes.STRING
        },
        categoryId : {
            type : DataTypes.INTEGER
        },
        activeStatus : {
            type : DataTypes.NUMBER
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return SubCategory;
}