// UserAddressInfo
let {DataTypes} = require('sequelize');
module.exports = (sql) => {
    let UserAddressInfo = sql.define('useraddressinfo',{
        user : {
            type : DataTypes.STRING
        },
        name : {
            type : DataTypes.STRING
        },
        address : {
            type : DataTypes.STRING
        },
        city : {
            type : DataTypes.STRING
        },
        state : {
            type  : DataTypes.STRING
        },
        zip : {
            type : DataTypes.STRING
        },
        activeStatus : {
            type : DataTypes.NUMBER
        },
        primaryValue : {
            type : DataTypes.NUMBER // 1 for primary , 0 or 2 for secondary
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return UserAddressInfo;
}
