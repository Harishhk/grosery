// UserContactInfo
let {DataTypes} = require('sequelize');
module.exports = (sql) => {
    let UserContactInfo = sql.define('usercontactinfo', {
        user: {
            type : DataTypes.NUMBER
        },
        name : {
            type : DataTypes.STRING
        },
        number : {
            type : DataTypes.NUMBER
        },
        freeze : {
            type : DataTypes.NUMBER
        },
        activeStatus : {
            type : DataTypes.NUMBER
        },
        primaryValue : {
            type : DataTypes.NUMBER // 1 for primary , 0 or 2 for secondary
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return UserContactInfo;
}