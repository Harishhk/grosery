const { DataTypes } = require('sequelize');

module.exports = (sql) => {
    let Banner = sql.define('banner' ,{
        name: {
            type : DataTypes.STRING
        },
        image : {
            type : DataTypes.STRING
        },
        bannerType : {
            type : DataTypes.NUMBER // 1 for products, 2 for statics
        },
        offer : {
            type : DataTypes.NUMBER // offer percentage
        },
        activeStatus : {
            type : DataTypes.NUMBER // 1 for acyive, 2 for inActive
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return Banner;
}