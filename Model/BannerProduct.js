const { DataTypes } = require('sequelize');

module.exports = (sql) => {
    let BannerProduct = sql.define('bannerproduct' ,{
        bannerId : {
            type: DataTypes.NUMBER
        },
        productId : {
            type: DataTypes.NUMBER
        },
        activeStatus : {
            type : DataTypes.NUMBER
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return BannerProduct;
}