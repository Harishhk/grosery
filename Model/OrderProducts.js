// OrderProducts
let {DataTypes} = require('sequelize');
module.exports = (sql) => {
    let OrderProducts = sql.define('orderproducts',{
        orderId : {
            type : DataTypes.NUMBER
        },
        productId : {
            type : DataTypes.NUMBER
        },
        quantity : {
            type : DataTypes.NUMBER
        },
        price : {
            type : DataTypes.NUMBER
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return OrderProducts;
}