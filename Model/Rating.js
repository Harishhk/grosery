// Ratings
const {DataTypes} = require('sequelize')
module.exports = (sql) => {
    const Rating = sql.define('rating', {
        orderId : {
            type: DataTypes.NUMBER
        },
        staffId : {
            type : DataTypes.NUMBER
        },
        userId : {
            type : DataTypes.NUMBER
        },
        rating : {
            type : DataTypes.NUMBER
        },
        review : {
            type : DataTypes.TEXT
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return Rating;
};