// ProductCategory
let {DataTypes} = require('sequelize');
module.exports = (sql) => {
    let ProductCategory = sql.define('productcategory', {
        product : {
            type: DataTypes.NUMBER
        },
        categoryId : {
            type: DataTypes.NUMBER
        },
        subcategoryId : {
            type: DataTypes.NUMBER
        },
        subCategoryTypeId : {
            type : DataTypes.NUMBER
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return ProductCategory;
}