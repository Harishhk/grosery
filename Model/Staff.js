// Staff.js
let { DataTypes }  = require('sequelize');
module.exports = (sql) => {
    let Staff = sql.define('staff', {
        firstName : {
            type : DataTypes.STRING
        },
        lastName : {
            type : DataTypes.STRING
        },
        profileImage : {
            type : DataTypes.STRING
        },
        email : {
            type : DataTypes.STRING
        },
        password : {
            type : DataTypes.STRING
        },
        phone : {
            type : DataTypes.NUMBER
        },
        age : {
            type : DataTypes.NUMBER
        },
        gender : {
            type : DataTypes.STRING
        },
        address : {
            type : DataTypes.STRING
        },
        deviceToken: {
            type: DataTypes.STRING
        },
        address1 : {
            type : DataTypes.STRING
        },
        city  :{
            type : DataTypes.STRING
        },
        state : {
            type : DataTypes.STRING
        },
        creditBalance : {
            type : DataTypes.NUMBER
        },
        zip  : {
            type : DataTypes.STRING
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return Staff
}