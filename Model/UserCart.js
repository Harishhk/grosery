// UserCart
let {DataTypes} = require('sequelize');
module.exports = (sql) => {
    let UserCart = sql.define('usercart', {
        userId : {
            type: DataTypes.NUMBER
        },
        productId : {
            type: DataTypes.NUMBER
        },
        quantity : {
            type: DataTypes.NUMBER
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return UserCart;
}