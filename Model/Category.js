// Category
const {DataTypes} = require('sequelize');
module.exports = (sql) => {
    let Category = sql.define('category',{
        name : {
            type : DataTypes.STRING
        },
        icon : {
            type : DataTypes.STRING
        },
        image : {
             type : DataTypes.STRING
        },
        title : {
             type : DataTypes.TEXT
        },
        subtitle : {
             type : DataTypes.TEXT
        },
        activeStatus : {
            type : DataTypes.NUMBER
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return Category;
}
