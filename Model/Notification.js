const {DataTypes} = require('sequelize');
module.exports = (sql) => {
    let Notification = sql.define( 'notification' ,{
        user : {
            type : DataTypes.NUMBER
        },
        message  : {
            type : DataTypes.TEXT
        },
        userType: {
            type : DataTypes.NUMBER
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return Notification;
}