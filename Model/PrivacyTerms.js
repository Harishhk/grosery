const { DataTypes } = require("sequelize");

module.exports = (sql) => {
    let PrivacyTerms = sql.define('privacyTerms', {
        text : {
            type : DataTypes.TEXT
        },
        status : {
            type : DataTypes.NUMBER // 1 for privacy, 2 for terms and condition
        },
        deleteStatus : {
            type : DataTypes.NUMBER
        }
    }, {
        timestamps : true,
        freezeTableName: true
    });
    return PrivacyTerms
}