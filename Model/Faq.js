const { DataTypes } = require("sequelize");

module.exports = (sql) => {
    let Faq = sql.define('faq', {
        question : {
            type : DataTypes.TEXT
        },
        answer : {
            type : DataTypes.TEXT
        },
        activeStatus : {
            type : DataTypes.NUMBER // 1 for active, 2 for in-active
        }
    }, {
        timestamps : true,
        freezeTableName: true
    });
    return Faq
}