let Sequelize = require('sequelize');
let sequelize = require('./db');
let db = {};
    db.Sequelize = Sequelize;
    db.sequelize = sequelize;

db.User = require('./User')(sequelize);
db.Admin = require('./Admin')(sequelize);
db.Category = require('./Category')(sequelize);
db.PrivacyTerms = require('./PrivacyTerms')(sequelize);
db.SubCategory = require('./SubCategory')(sequelize);
db.UserAddressInfo = require('./UserAddressInfo')(sequelize);
db.UserContactInfo = require('./UserContactInfo')(sequelize);
db.Product = require('./Product')(sequelize);
db.ProductCategory = require('./ProductCategory')(sequelize);
db.ProductImage = require('./ProductImage')(sequelize);
db.UserCart = require('./UserCart')(sequelize);
db.Duration = require('./Duration')(sequelize);
db.SubCategoryType = require('./SubCategoryType')(sequelize);
db.Order = require('./Order')(sequelize);
db.OrderProducts = require('./OrderProducts')(sequelize);
db.Offer = require('./Offer')(sequelize);
db.Staff = require('./Staff')(sequelize);
db.StaffOrder = require('./StaffOrder')(sequelize);
db.Banner = require('./Banner')(sequelize);
db.BannerProduct = require('./BannerProduct')(sequelize);
db.Rating = require('./Rating')(sequelize);
db.Notification = require('./Notification')(sequelize);
db.Faq = require('./Faq')(sequelize);

// Assosian:
// Subcategory
db.Category.hasMany(db.SubCategory, {sourceKey: 'id', foreignKey: 'categoryId' });
db.SubCategory.belongsTo(db.Category, {foreignKey: 'categoryId'} );

db.User.hasMany(db.UserContactInfo, {sourceKey: 'id', foreignKey: 'user'});
db.User.hasMany(db.UserAddressInfo, {sourceKey: 'id', foreignKey: 'user'});
db.Product.hasMany(db.ProductCategory, {sourceKey: 'id', foreignKey: 'product'});
db.Product.hasMany(db.ProductImage, {sourceKey: 'id', foreignKey: 'product'});
db.Category.hasMany(db.ProductCategory, {sourceKey: 'id', foreignKey: 'categoryId'});
db.ProductCategory.belongsTo(db.Category, {foreignKey: 'categoryId'} );
db.ProductCategory.belongsTo(db.SubCategory, {foreignKey: 'subcategoryId'} );
db.SubCategory.hasMany(db.ProductCategory, {sourceKey: 'id', foreignKey: 'subcategoryId'});

db.Product.hasMany(db.UserCart, {sourceKey: 'id', foreignKey: 'productId'});
db.User.hasMany(db.UserCart, {sourceKey: 'id', foreignKey: 'userId'});
db.UserCart.belongsTo(db.User, {foreignKey: 'userId'});
db.UserCart.belongsTo(db.Product, {foreignKey: 'productId'});

db.SubCategory.hasMany(db.SubCategoryType, {sourceKey: 'id', foreignKey: 'subCategoryId'});
db.SubCategoryType.belongsTo(db.SubCategory, {foreignKey: 'subCategoryId'});
db.ProductCategory.belongsTo(db.SubCategoryType, {foreignKey: 'subCategoryTypeId'});

// Order Page 
db.User.hasMany(db.Order, {sourceKey: 'id', foreignKey: 'userId'});
db.Order.belongsTo(db.User, {foreignKey: 'userId'});
db.Duration.hasMany(db.Order, {sourceKey: 'id', foreignKey: 'durationId'});
db.Order.belongsTo(db.Duration, {foreignKey: 'durationId'});
db.Offer.hasMany(db.Order, {sourceKey : 'id', foreignKey: 'voucher'});
db.Order.belongsTo(db.Offer, {foreignKey: 'voucher'});
db.Order.hasMany(db.OrderProducts, {sourceKey: 'id', foreignKey: 'orderId'});
db.OrderProducts.belongsTo(db.Order, {foreignKey: 'orderId'});
db.Product.hasMany(db.OrderProducts, {sourceKey: 'id', foreignKey: 'productId'});
db.OrderProducts.belongsTo(db.Product, {foreignKey: 'productId'});
db.UserAddressInfo.hasMany(db.Order, {sourceKey: 'id', foreignKey: 'address'});
db.Order.belongsTo(db.UserAddressInfo, {foreignKey: 'address'});

db.UserContactInfo.hasMany(db.Order, {sourceKey: 'id', foreignKey: 'contactId'});
db.Order.belongsTo(db.UserContactInfo, {foreignKey: 'contactId'});

db.StaffOrder.belongsTo(db.Order, {foreignKey: 'orderId'});
db.StaffOrder.belongsTo(db.Staff, {foreignKey: 'staffId'});
db.Order.hasMany(db.StaffOrder, {sourceKey: 'id' ,foreignKey: 'orderId'});
db.Staff.hasMany(db.StaffOrder, {sourceKey: 'id' ,foreignKey: 'staffId'});

// Banner Product Links 
db.Banner.hasMany(db.BannerProduct, {sourceKey: 'id', foreignKey: 'bannerId'});
db.Product.hasMany(db.BannerProduct, {sourceKey: 'id', foreignKey: 'productId'});
db.BannerProduct.belongsTo(db.Banner, {foreignKey: 'bannerId'});
db.BannerProduct.belongsTo(db.Product, {foreignKey: 'productId'});

// Ratings 
db.User.hasOne(db.Rating, {sourceKey: 'id', foreignKey: 'userId'});
db.Staff.hasOne(db.Rating, {sourceKey: 'id', foreignKey: 'staffId'});
db.Order.hasOne(db.Rating, {sourceKey: 'id', foreignKey: 'orderId'});
db.Rating.belongsTo(db.User, {foreignKey: 'userId'});
db.Rating.belongsTo(db.Staff, {foreignKey: 'staffId'});
db.Rating.belongsTo(db.Order, {foreignKey: 'orderId'});

module.exports = db;
