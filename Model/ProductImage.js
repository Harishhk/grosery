let {DataTypes} = require('sequelize');
module.exports = (sql) => {
    let ProductImage = sql.define('productimage', {
        product : {
            type: DataTypes.NUMBER
        },
        image : {
            type : DataTypes.STRING
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return ProductImage;
}