// StaffOrder
const  { DataTypes } = require('sequelize');
module.exports = (sql) => {
    let StaffOrder = sql.define('stafforder',{
        staffId: {
            type : DataTypes.NUMBER
        },
        orderId : {
            type : DataTypes.NUMBER
        },
        status : {
            type : DataTypes.NUMBER // 0 for pending , 1 for accept, 2 decline, 3 for Compleated
        }
    },{
        timestamps : true,
        freezeTableName: true
    });
    return StaffOrder;
}