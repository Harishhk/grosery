let http = require('http');
let express = require('express');
let app = express();
let bodyParser = require('body-parser');
let cors = require('cors');
const routes = require('./Routes');
let config = require('./Config')
app.use(cors());
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
app.use(express.static('public'));
app.use('/api', routes);
app.use('*', (req, res) => {
    res
    .status(404)
    .json( config.notFound );
});
const port = process.env.PORT || 5526
http.createServer(app).listen(port, function(){
    console.log(`app is running in localhost:${port}`)
});