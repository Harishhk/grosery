//UploadService.js
var multer = require('multer');
let config = require('../Config')
const path = require("path");
const fs = require("fs");

var storage = multer.diskStorage({ 
    destination: function (req, file, cb) { 
        var dir = "./public/uploadImage" 
        cb(null, dir);
    }, 
    filename: function (req, file, cb) {
        console.log(file.originalname);
        let fileExe = file.originalname.split('.').slice(1).pop();
        console.log(fileExe)
        cb(null, file.fieldname + "-" + Date.now()+`.${fileExe}`) 
    } 
});


module.exports = {
    importImage : (req, res, folder) => {
        if (!fs.existsSync('./public/uploadImage')){
            fs.mkdirSync('./public/uploadImage');
        }
        let upload = multer({
            dest: "./public/uploadImage"
            // storage: storage
        }).single('image');
        upload(req, res ,function(err) {
            const tempPath = req.file.path;
            var exe = path.extname(req.file.originalname);
            console.log(exe)
            var file = req.file.fieldname + "-" + Date.now()+`${exe}`
            const targetPath = path.join(`./public/uploadImage/${file}`);

            // if (path.extname(req.file.originalname).toLowerCase() === ".png") {
            fs.rename(tempPath, targetPath, err => {
                if (err) console.log(err);
                let url = `${config.imgBaseUrl}uploadImage/${file}`
                res.status(200).send({apiStatus : true, statusCode : 200, result : url, message : 'Uploaded sucessfully!'});
            });
            // } else {
            // fs.unlink(tempPath, err => {
            //     if (err) return handleError(err, res);

            //     res
            //     .status(403)
            //     .contentType("text/plain")
            //     .end("Only .png files are allowed!");
            // });
            // }
        })
    }
}