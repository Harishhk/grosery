// jwt.js
// 0 for Admin and 1 for User and 2 for staff
let jwt = require('jsonwebtoken');
const { noAccess } = require('../Config');
require('dotenv').config();
var Secret = process.env.JwtSecret || 'grocery';
module.exports = {
	generate : async (obj) => {
		try {
			let token = await jwt.sign(obj, Secret);
            return token;
		} catch (error) {
			console.log(error);
            return false
		}
	},
	verify : async (req, res, next) => {
		try {
			if(!req.headers || !req.headers.authorization){
                return res.send(noAccess);
            }
            var token = req.headers.authorization;
            var n = token.search("Bearer ");
            if (n < 0) return res.send(noAccess);
            token = token.replace('Bearer ', '');
            let verify = jwt.verify(token, Secret);
            if (!verify || !verify.id){
                return res.send(noAccess);
            }
            req.token = verify;
            console.log(verify)
            next();
		} catch (error) {
			console.log(error)
			return res.send(noAccess);
		}
	},
	verifyAdmin : async (req, res, next) => {
		try {
			if(!req.headers || !req.headers.authorization){
                return res.send(noAccess);
            }
            var token = req.headers.authorization;
            var n = token.search("Bearer ");
            if (n < 0) return res.send(noAccess);
            token = token.replace('Bearer ', '');
            let verify = jwt.verify(token, Secret);
            if (!verify || !verify.id){
                return res.send(noAccess);
            }
            if (verify.user == 1 || verify.user == 2) return res.send(noAccess);
            req.token = verify;
            console.log(verify)
            next();
		} catch (error) {
			console.log(error)
			return res.send(noAccess);
		}
	},
}