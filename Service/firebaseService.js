var admin = require("firebase-admin");
var serviceAccount = require('../grocery-759e1-firebase-adminsdk-jcb1q-29411ea083.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://grocery-759e1-default-rtdb.firebaseio.com"
});
var fbDb = admin.database();
module.exports = {
    pushNotification : async (message, deviceToken) => {
        var notify = {
            notification : {
                title : 'Grosery',
                body : message,
                // image: "https://www.zappfresh.com/images/self/zappfresh-logo.png",
            }, 
            // data : {
            //     click_action : "FLUTTER_NOTIFICATION_CLICK"
            // },
            token : deviceToken
        }
        admin.messaging().send(notify).then((resp) => {
            console.log('successfully send message : ', resp)
        }).catch((err) => {
            console.log('Not sucess : ', err)
        })
    },
    addUserNotification : (user) => {
        try {
            fbDb.ref(`user/${user}`).once("value", function(snapshot) {
                let val = snapshot.val() || 0;
                let ts = val + 1
                fbDb.ref(`user/${user}`).set(ts);
                return true;
            })
        } catch (error) {
            console.log(error);
            return false;
        }
    },
    addStaffNotification : (user) => {
        try {
            fbDb.ref(`staff/${user}`).once("value", function(snapshot) {
                let val = snapshot.val() || 0;
                let ts = val + 1
                fbDb.ref(`staff/${user}`).set(ts);
                return true;
            })
        } catch (error) {
            console.log(error);
            return false;
        }
    }
}