let bcrypt = require('bcryptjs');

module.exports = {
    generate : async (password) => {
        try {
            if (!password) return false;
            let hash = bcrypt.hashSync(password, 10);
            return hash;
        } catch (error) {
            console.log(error);
            return false
        }
    },
    verify : async (password, dbPassword) => {
        try {
            if(!password || !dbPassword) return false;
            let compare = bcrypt.compareSync(password, dbPassword);
            return compare;
        } catch (error) {
            console.log(error);
            return false;
        }
    }
}