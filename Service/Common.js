const { Op } = require("sequelize");
const db = require("../Model")

module.exports = {
    categoryChecker : async (arr) => {
        try {
            let resp = true;
            for(let obj of arr) {
                let finder = await db.Category.count({where : {id: obj.category}});
                if (finder == 0) resp = false;
                let finder2 = await db.SubCategory.count({where : {id: obj.subcategory, categoryId: obj.category}});
                if (finder2 == 0) resp = false;
                if (obj.type.length > 0) {
                    for(let ap of obj.type){
                        let finder3 = await db.SubCategoryType.count({where : {id: ap.id, subCategoryId: obj.subcategory}});
                        if (finder3 == 0) resp = false;
                    }
                } else {
                    resp = false;
                }
            }
            return resp;
        } catch (error) {
            console.log(error);
            return false;
        }
    },
    validator : async (arr, val) => {
        try {
            let resp = true;
            for(let obj of arr) {
                if (!obj[val] || obj[val] == '') resp = false;
            }
            return resp;
        } catch (error) {
            console.log(error);
            return false;
        }
    },
    productChecker : async (product) => {
        try {
            let ds = false;
            let message = '';
            let product1 = 0;
            for (let val of product) {
                if (!val.productId || !val.quantity || !val.price) {
                    ds = true;
                    message = 'Mandatory field is missing' ;
                    product1 = val.productId;
                    break;
                }
                let ck = await db.Product.count({where: {
                    id: val.productId,
                    stock : {
                        [Op.gte] : val.quantity
                    }
                }});
                if (ck == 0) {
                    ds = true;
                    product1  = val.productId;
                    message = 'Product or quantity exceeded!';
                    break;
                }
            }
            return {error : ds, message: message, product: product1}
        } catch (error) {
            console.log(error);
            return {error : true, message : 'Something went wrong!', product : 0}
        }
    },
    bannerProductCheck : async (bannerProducts) => {
        try {
            let ts = true;
            for(let val of bannerProducts) {
                let ck = await db.Product.count({where : {id: val.id}});
                if(ck == 0) ts = false;
            }
            return {result: ts}
        } catch (error) {
            console.log(error);
            return {result : false}
        }
    },
    bannerEditProductCheck : async (bannerProducts, bannerId) => {
        try {
            let arr = []
            let ts = false;
            for(let val of bannerProducts) {
                let obj = {}
                let ck = await db.Product.count({where : {id: val.id}});
                if(ck == 0) ts = true;
                let ck2 = await db.BannerProduct.count({where : {bannerId: bannerId, productId : val.id}});
                if (ck2 == 0) {
                    arr.push({id: val.id});
                }
            }
            return {result : arr, error: ts}
        } catch (error) {
            console.log(error);
            return {result : [], error : true}
        }
    }
}