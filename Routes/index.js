const express = require('express');
const Router = express.Router();
const Controller = require('../Controller');
const jwt = require('../Service/jwt');

Router.get('/', async (req, res) => {
    res.send({message: 'Api is running'})
});

// User Apis
Router.post('/signUp', Controller.UserController.signUp);
Router.post('/login', Controller.UserController.login);

// common Apis
Router.put('/changePassword',jwt.verify, Controller.AdminController.changePassword);

// Admin Apis
Router.post('/createAdmin', Controller.AdminController.createAdmin);
Router.post('/Adminlogin', Controller.AdminController.Adminlogin);
Router.get('/listUser', jwt.verifyAdmin, Controller.AdminController.listUser);

// Privacy & Terms
Router.get('/listPrivacy', Controller.PrivacyTermsController.listPrivacy);
Router.get('/listTerms', Controller.PrivacyTermsController.listTerms);
Router.post('/updatePrivacyTerms', jwt.verifyAdmin, Controller.PrivacyTermsController.updatePrivacyTerms);

// Category
Router.post('/createCategory', jwt.verifyAdmin, Controller.CategoryController.createCategory);
Router.get('/listCategory', Controller.CategoryController.listCategory);
Router.put('/editCategory',jwt.verifyAdmin , Controller.CategoryController.editCategory);

// SubCategory
Router.post('/createSubcategory',jwt.verifyAdmin , Controller.CategoryController.createSubcategory);
Router.get('/listSubCategory', Controller.CategoryController.listSubCategory);
Router.put('/editSubCategory', jwt.verifyAdmin, Controller.CategoryController.editSubCategory);

// SubCategory Type 
Router.post('/createSubCategoryType',jwt.verifyAdmin , Controller.CategoryController.createSubCategoryType);
Router.get('/listSubcategoryType', Controller.CategoryController.listSubcategoryType);
Router.put('/editSubCategoryType', jwt.verifyAdmin, Controller.CategoryController.editSubCategoryType);

// Profile
Router.get('/listProfile', jwt.verify, Controller.ProfileController.listProfile);
Router.post('/createUserContact', jwt.verify, Controller.ProfileController.createUserContact);
Router.put('/updateUserContact', jwt.verify, Controller.ProfileController.updateUserContact);
Router.put('/removeContact', jwt.verify, Controller.ProfileController.removeContact);
Router.post('/createUserAddressInfo', jwt.verify, Controller.ProfileController.createUserAddressInfo);
Router.put('/updateUserAddressInfo', jwt.verify, Controller.ProfileController.updateUserAddressInfo);
Router.put('/removeAddress', jwt.verify, Controller.ProfileController.removeAddress);

// UploadImages
Router.post('/uploadImage', jwt.verify, Controller.AdminController.uploadImage);

// Produt Api's
Router.post('/createProduct', jwt.verifyAdmin, Controller.ProductController.createProduct);
Router.get('/listProduct', Controller.ProductController.listProduct);
Router.put('/editProduct', Controller.ProductController.editProduct)

// Cart 
Router.post('/addToCart', jwt.verify, Controller.CartController.addToCart);
Router.get('/listUserCart', jwt.verify, Controller.CartController.listUserCart);
Router.put('/editCart', jwt.verify, Controller.CartController.editCart);
Router.put('/removeCart', jwt.verify, Controller.CartController.removeCart);

// Duration
Router.get('/listDuration', Controller.AdminController.listDuration);
Router.post('/createDuration', jwt.verifyAdmin, Controller.AdminController.createDuration);
Router.put('/editDuration', jwt.verifyAdmin, Controller.AdminController.editDuration);
// Extra Products
Router.get('/listUserProduct', Controller.ProductController.listUserProduct);
Router.get('/listProductDetails', Controller.ProductController.listProductDetails);

// Offers
Router.post('/createOffer', jwt.verifyAdmin ,Controller.OfferController.createOffer);
Router.put('/editOffers', jwt.verifyAdmin ,Controller.OfferController.editOffers);
Router.get('/listOffer', Controller.OfferController.listOffer);
Router.get('/offerCodeChecker', jwt.verify ,Controller.OfferController.offerCodeChecker);

// Orders 
Router.post('/createOrder', jwt.verify, Controller.OrderController.createOrder);
Router.get('/listOrder', jwt.verify, Controller.OrderController.listOrder);
Router.put('/assignOrder', jwt.verifyAdmin, Controller.OrderController.assignOrder);
Router.get('/listMyOrder', jwt.verify, Controller.OrderController.listMyOrder);

Router.put('/staffOrderConfiramtion', jwt.verify, Controller.OrderController.staffOrderConfiramtion);
Router.post('/OrderRecieved', jwt.verify, Controller.OrderController.OrderRecieved);

// Staff
Router.post('/createStaff', jwt.verifyAdmin, Controller.StaffController.createStaff);
Router.get('/listStaff', jwt.verifyAdmin, Controller.StaffController.listStaff);
Router.put('/editStaffData', jwt.verify, Controller.StaffController.editStaffData);
Router.post('/loginStaff', Controller.StaffController.loginStaff);
Router.get('/staffDetails', jwt.verify, Controller.StaffController.staffDetails);
Router.get('/staffDashboard', jwt.verify, Controller.StaffController.staffDashboard);

// Banner
Router.post('/createBanner', jwt.verify, Controller.BannerController.createBanner);
Router.get('/listBanner', Controller.BannerController.listBanner);
Router.put('/editBanner', Controller.BannerController.editBanner);

// Ratings
Router.get('/listRatings', jwt.verify, Controller.RatingControlller.listRatings);
Router.post('/addRatings', jwt.verify, Controller.RatingControlller.addRatings);
Router.get('/listOrderDetails', jwt.verify, Controller.RatingControlller.listOrderDetails);

// Notification
Router.get('/listNotification', jwt.verify, Controller.NotificationController.listNotification);
Router.get('/logOut', jwt.verify, Controller.NotificationController.logOut);

// Faq Apis
Router.post('/createFaq', jwt.verifyAdmin, Controller.FaqController.createFaq);
Router.put('/editFaq', jwt.verifyAdmin, Controller.FaqController.editFaq);
Router.get('/listFaq', Controller.FaqController.listFaq);

module.exports = Router;