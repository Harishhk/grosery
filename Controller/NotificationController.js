// NotificationController
const { mandatoryError, somethingWentWrong } = require('../Config');
const {Op} = require('sequelize');
let db = require('../Model');
module.exports = {
    listNotification : async (req, res) => {
        try {
            let {id, user} = req.token;
            let {page, limit} = req.query;
            page = parseInt(page) || 1;
            limit = parseInt(limit) || 10;
            let skip = (page - 1) * limit;
            let searchQuery = {
                where : {
                    userType :user,
                    user : id
                },
                offset : skip, 
                limit: limit,
                order: [
                    ['createdAt', 'DESC']
                ]
            }
            let cc = await db.Notification.count({where : searchQuery.where});
            let abs = cc/limit;
            let finder = await db.Notification.findAll(searchQuery);
            return res.send({apiStatus: true, statusCode: 200, result : finder, count: Math.ceil(abs), message: 'Data Found!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    logOut : async (req, res) => {
        try {
            const {id, user} = req.token;
            if (user == 1) await db.User.update({deviceToken: null}, {where : {id: id}});
            if (user == 2) await db.Staff.update({deviceToken: null}, {where : {id: id}});
            return res.send({apiStatus: true, statusCode : 200, result: {}, message: 'Updated!'})
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    }
}