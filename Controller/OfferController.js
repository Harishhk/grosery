const {Op} = require('sequelize');
const db = require('../Model');
const { somethingWentWrong, mandatoryError, noAccess } = require("../Config");
const moment = require('moment');
module.exports = {
    createOffer : async (req, res) => {
        try {
            const {name, percentage, description, image, activeState, offerCode, expiresdate} = req.body;
            if (!name || !percentage || !offerCode || !expiresdate) return res.send(mandatoryError);
            if (parseFloat(percentage) > 100) return res.send({apiStatus: false, statusCode : 400, result : {}, message : 'Percentage must be within 100'});
            let vouchCk = await db.Offer.count({where : {offerCode : offerCode.toLowerCase()}});
            if (vouchCk > 0) return res.send({apiStatus: 400, statusCode: false, result : {}, message : 'Offer Code already exists!'});
            let dateChecker = moment(expiresdate).isValid();
            if (dateChecker == false) return res.send({apiStatus : false, statusCode :400, result: {}, message : 'Date is invalid!'});
            let createQuery = {
                name : name,
                percentage : parseFloat(percentage),
                activeState : activeState || 1,
                image: image || '',
                expiresdate: moment(expiresdate).format(),
                offerCode : offerCode.toLowerCase(),
                description : description || ''
            };
            let crt  = await db.Offer.create(createQuery);
            return res.send({apiStatus: true, statusCode: 200, message: 'Data Created', result: crt});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    listOffer : async (req, res) => {
        try {
            const {activeState} = req.query;
            let {page, limit} = req.query;
            page = parseInt(page) || 1;
            limit = parseInt(limit) || 10;
            let skip = (page - 1) * limit;
            let searchQuery = {
                where : {},
                offset : skip, 
                limit: limit,
                order: [
                    ['createdAt', 'DESC']
                ],
            }
            if (activeState && activeState != '') {
                searchQuery.where = {
                    activeState : activeState,
                    expiresdate : {
                        [Op.gte] : moment().startOf('day').format()
                    }
                }
            }
            let cc = await db.Offer.count({where : searchQuery.where});
            let search = await db.Offer.findAll(searchQuery);
            let act = cc / limit
            return res.send({apiStatus: true, statusCode : 200, result: search, count : Math.ceil(cc) ,message: 'Data found!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    editOffers : async (req, res) => {
        try {
            const {id, name, percentage, description, image, offerCode, activeState} = req.body
            if (!id || (!name && !percentage && !description && !offerCode && !image && !activeState)) return res.send(mandatoryError);
            let cheker = await db.Offer.count({where : {id: id}});
            if(cheker == 0) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'Offer not found!' });
            let updateQuery = {};
            if (name && name != '') updateQuery.name = name;
            if (percentage && percentage != '') updateQuery.percentage = parseFloat(percentage);
            if (description && description != '') updateQuery.description = description;
            if (image && image != '') updateQuery.image = image;
            if (activeState && activeState != '') {
                if (parseInt(activeState) == 1) {
                    updateQuery.activeState = 1;
                } else {
                    updateQuery.activeState = 2;
                }
            }
            if (offerCode && offerCode != '') {
                let vouchCk = await db.Offer.count({where : {offerCode : offerCode.toLowerCase(), id : {
                    [Op.ne] : id
                }}});
                if (vouchCk > 0) {
                    return res.send({apiStatus: 400, statusCode: false, result : {}, message : 'Offer Code already exists!'});
                } else {
                    updateQuery.offerCode = offerCode;
                }
            }
            let up = db.Offer.update(updateQuery, {where: {id: id}});
            return res.send({apiStatus: true, statusCode: 200, message : 'Data updated', result: {}});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    offerCodeChecker : async (req, res) => {
        try {
            const { offercode } = req.query;
            if(!offercode || offercode == '') return res.send({apiStatus: false, statusCode : 400, message: 'Enter the voucher code to apply', result: {}});
            let offerCk = await db.Offer.findOne({where: {offerCode: offercode, activeState: 1}});
            if (!offerCk) return res.send({apiStatus: false, statusCode: 400, result : {}, message: 'Offer not Found or Expired!'});
            return res.send({apiStatus: true, statusCode: 200, message: 'Offer Found', result : offerCk});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    }
}