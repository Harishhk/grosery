// CategoryController
const { mandatoryError, somethingWentWrong } = require('../Config');
const {Op} = require('sequelize');
let db = require('../Model')
module.exports = {
    createCategory : async (req, res) => {
        try {
            const {name, icon, activeStatus, image, title, subtitle} = req.body;
            if (!name || !icon) return res.send(mandatoryError);
            let finder = await db.Category.count({where : {
                name: name
            }});
            if (finder > 0) return res.send({apiStatus : false, statusCode : 400, result: {}, message : 'Category already exists!' })
            let createQuery = {
                name,
                icon,
                image : image || null,
                title : title || '',
                subtitle : subtitle || ''
            };
            if (activeStatus && activeStatus != '') createQuery.activeStatus = parseInt(activeStatus);
            else createQuery.activeStatus = 1;
            let crt = await db.Category.create(createQuery);
            return res.send({apiStatus : true, statusCode : 200, result : crt, message: 'Created Sucesssfully!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    editCategory : async (req, res) => {
        try {
            const {id, name, icon, image, title, subtitle} = req.body;
            if (!id ||(!name && !icon & !image & !title & !subtitle)) return res.send(mandatoryError);
            let checker = await db.Category.count({id : id});
            if(checker == 0) return res.send({apiStatus: false, statusCode: 400, result: {}, mmessage: 'Category Not Found!'});
            let updateQuery = {}
            if (name && name != '') updateQuery.name = name;
            if (icon && icon != '') updateQuery.icon = icon;
            if (image && image != '') updateQuery.image = image;
            if (title && title != '') updateQuery.title = title;
            if (subtitle && subtitle != '') updateQuery.subtitle = subtitle;
            let up = await db.Category.update(updateQuery, {where: {id: id}});
            return res.send({apiStatus: true, statusCode: 200, message: 'Updated Sucessfully!', result: up});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    listCategory : async (req, res) => {
        try {
            let finder = await db.Category.findAll({where: {}, include : [db.SubCategory] });
            return res.send({apiStatus: true, statusCode: 200 , result: finder, message: 'Data Found!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    createSubcategory : async (req, res) => {
        try {
            const {name, icon, category} = req.body;
            if (!name || !category) return res.send(mandatoryError);
            let checker = await db.Category.count({id: category});
            if (checker < 1) return res.send({apiStatus: false, statusCode: 400, result : {}, message: 'Category not found'});
            let createQuery = {
                categoryId: category,
                name: name,
                activeStatus: 1,
                icon: icon || ''
            };
            let crt = await db.SubCategory.create(createQuery);
            return res.send({apiStatus: true, statusCode: 200, result: crt, message: 'Data Created!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    editSubCategory : async (req, res) => {
        try {
            const {id, name, icon, category} = req.body;
            if (!id ||(!name && !icon && !category)) return res.send(mandatoryError);
            let checker = await db.SubCategory.count({id: id});
            if (checker == 0) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'Subcategory Not Found!'});
            let updateQuery = {

            };
            if (name && name != '') updateQuery.name = name;
            if (icon && icon != '') updateQuery.icon = icon;
            if (category && category != '') {
                let ch2 = await db.Category.count({id : id});
                if (ch2 == 0) return res.send({apiStatus: false, statusCode: 400, result : {}, message: 'Category not found'});
                updateQuery.categoryId = category;
            }
            let up = await db.SubCategory.update(updateQuery, {where: {id: id}});
            return res.send({apiStatus: true, statusCode: 200, result: up, message: 'Updated Sucessfully!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    listSubCategory : async (req, res) => {
        try {
            const {categoryId, activeStatus} = req.query;
            let searchQuery = {}
            if (categoryId && categoryId != '') {
                let ck = await db.Category.count({where : {id: categoryId}});
                if (ck == 0) return res.send({apiStatus: false, statusCode: 200, result: {}, message: 'Category Not Found!'});
                searchQuery.categoryId = categoryId
            }
            if (activeStatus && activeStatus != '') {
                searchQuery.activeStatus = parseInt(activeStatus)
            }
            const finder = await db.SubCategory.findAll({where: searchQuery, include: [db.Category, db.SubCategoryType]});
            return res.send({apiStatus: true, statusCode: 200 , result: finder, message: 'Data Found!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    createSubCategoryType : async (req, res) => {
        try {
            const {SubCategory, name} = req.body;
            if(!SubCategory || !name) return res.send(mandatoryError);
            let ck = await db.SubCategory.count({where : {id: SubCategory}});
            if (ck == 0) return res.send({apiStatus: false, statusCode: 400, message: 'Sub Category Not found!', result: {}})
            let crt = await db.SubCategoryType.create({name: name, subCategoryId: SubCategory, activeStatus: 1});
            return res.send({apiStatus: true, statusCode: 200, result: crt, message: 'Created Sucessfully!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    listSubcategoryType : async (req, res) => {
        try {
            const {SubCategory, search, activeStatus} = req.query;
            let {page, limit} = req.query;
            page = parseInt(page) || 1;
            limit = parseInt(limit) || 10;
            let skip = (page - 1) * limit;
            let searchQuery = {};
            if (search && search != '') {
                searchQuery = {name: {[Op.like] : `%${search}%`}}
            }
            if (activeStatus && activeStatus != '') {
                searchQuery.activeStatus = parseInt(activeStatus);
            }
            if (SubCategory && SubCategory != '') {
                let ck = await db.SubCategory.count({where: {id: SubCategory}});
                if (ck == 0) return res.send({apiStatus: 400, statusCode: false, result: {}, message : 'Sub Category not found!'});
                searchQuery.subCategoryId = SubCategory;
            }
            let cc = await db.SubCategoryType.count({where : searchQuery});
            let finder = await db.SubCategoryType.findAll({where: searchQuery, include: [db.SubCategory], offset : skip, limit: limit});
            return res.send({apiStatus: true, statusCode: 200, message: 'Data Found!', count: cc, result: finder})
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    editSubCategoryType : async (req, res) => {
        try {
            const {TypeId, name, activeStatus} = req.body;
            if(!TypeId || (!name && !activeStatus)) return res.send(mandatoryError);
            let ckc = await db.SubCategoryType.count({id: TypeId});
            if(ckc == 0) return res.send({apiStatus: true, statusCode: 200, result: {}, message: 'No Type found'});
            let updateQuery = {};
            if (name && name != '') updateQuery.name = name;
            if (activeStatus && activeStatus != '') updateQuery.activeStatus = parseInt(activeStatus);
            let up = await db.SubCategoryType.update(updateQuery, {where: {id: TypeId}});
            return res.send({apiStatus: true, statusCode: 200, result: {}, message: 'Data Updated!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
}
