const { somethingWentWrong, mandatoryError } = require('../Config');
const db = require('../Model');

module.exports = {
    createFaq : async (req, res) => {
        try {
            let {id, user} = req.token;
            const {question, answer, activeStatus} = req.body;
            if (!question || !answer) return res.send(mandatoryError);
            let createQuery = {
                question : question,
                answer : answer
            }
            if (activeStatus && activeStatus != '') {
                if (parseInt(activeStatus) == 1) {
                    updateQuery.activeStatus == parseInt(activeStatus);
                } else {
                    updateQuery.activeStatus == 2;
                }
            }
            else createQuery.activeStatus = 1;
            let crt = await db.Faq.create(createQuery);
            return res.send({apiStatus: true, statusCode : 200, result : crt, message : 'created Sucessfully!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    listFaq : async (req, res) => {
        try {
            let {page, limit} = req.query;
            page = parseInt(page) || 1;
            limit = parseInt(limit) || 10;
            let skip = (page - 1) * limit;
            let searchQuery = {
                where : {},
                offset : skip, 
                limit: limit
            }
            let cc = await db.Faq.count();
            let finder = await db.Faq.findAll(searchQuery);
            let abs = cc / limit;
            return res.send({apiStatus :true, statusCode: 200, result: finder, count: Math.ceil(abs), })
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    editFaq : async (req, res) => {
        try {
            let {id, user} = req.token;
            const {faqId, question, answer, activeStatus} = req.body;
            if (!faqId || (!question && !answer && !activeStatus)) return res.send(mandatoryError);
            let cker = await db.Faq.count({where : {id: faqId}});
            if (cker == 0) return res.send({apiStatus: false, statusCode: 400, message : 'No Data Found to update', result: {}});
            let updateQuery = {};
            if (question && question != '') updateQuery.question == question;
            if (answer && answer != '') updateQuery.answer == answer;
            if (activeStatus && activeStatus != '') {
                if (parseInt(activeStatus) == 1) {
                    updateQuery.activeStatus == parseInt(activeStatus);
                } else {
                    updateQuery.activeStatus == 2;
                }
            }
            await db.Faq.update(updateQuery, {where : {id: faqId}});
            return res.send({apiStatus: true, statusCode: 200, message: 'Data Updated', result: {}});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    }
}