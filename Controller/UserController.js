const { Op } = require("sequelize");
const { mandatoryError, somethingWentWrong } = require('../Config');
const db = require('../Model');
let  PasswordService = require('../Service/PasswordService');
let jwt = require('../Service/jwt')
module.exports = {
    signUp : async (req, res) => {
        try {
            let {firstName, lastName, email, password, phone, deviceToken} = req.body;
            if (!firstName || !email || !password || !phone) return res.send(mandatoryError);
            email = email.toLowerCase();
            let emailChecker = await db.User.count({where : {
                [Op.or] : [
                    {
                        email: email
                    },{
                        phone: phone
                    }
                ]
            }});
            if (emailChecker > 0) return res.send({apiStatus: false, statusCode : 400, result: {}, message : 'Email or phone already exist!'});
            let hash = await PasswordService.generate(password);
            if (hash == false) return res.send(somethingWentWrong);
            let createQuery = {
                firstName,
                email,
                phone: phone,
                password: hash,
                deleteStatus: 0
            }
            if (deviceToken && deviceToken != '') createQuery.deviceToken = deviceToken;
            if (lastName && lastName != '') createQuery.lastName = lastName;
            let crt = await db.User.create(createQuery);
            let ct = await db.UserContactInfo.create({user: crt.id, name : 'Primary', number: phone, freeze : 1, activeStatus: 1, primaryValue: 1});
            let token = await jwt.generate({id: crt.id, user : 1});
            return res.send({apiStatus: true, statusCode: 200, result: crt, token ,message: 'Created Sucessfully!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    login : async (req, res) => {
        try {
            let {email, password, deviceToken} = req.body;
            if (!email || !password) return res.send(mandatoryError)
            // email = email.toLowerCase()
            let checker = await db.User.findOne({
                where : {
                    [Op.or] : [
                        {
                            email: email.toLowerCase(),
                        },
                        {
                            phone: email,
                        }
                    ],
                }
            });
            if (!checker) return res.send({apiStatus: false, statusCode: 400, result: {}, message: "User not found" });
            let passChecker = await PasswordService.verify(password, checker.password);
            if(passChecker == false) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'Password Missmatches!'});
            if (deviceToken && deviceToken != '') {
                await db.User.update({deviceToken: deviceToken},{where: {id: checker.id}})
            }
            let token = await jwt.generate({id: checker.id, user : 1});
            return res.send({apiStatus: true, statusCode: 200, result: checker, token, message: 'Data Found!'})
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
}