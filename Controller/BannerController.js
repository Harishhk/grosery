const { rest } = require('lodash');
const { somethingWentWrong, mandatoryError } = require('../Config');
const { bannerProductCheck, bannerEditProductCheck } = require('../Service/Common')
const db = require('../Model');

module.exports = {
    createBanner : async (req, res) => {
        try {
            const {id, user} = req.token;
            const {name, image, bannerType, offer, bannerProducts} = req.body;
            if(!image || !bannerType) return res.send(mandatoryError);
            if (bannerType != 1 && bannerType != 2) return res.send({apiStatus: false, statusCode : 400, message : 'Banner Type not found!', result : {}});
            if(bannerType == 1) {
                if(!offer || !bannerProducts || bannerProducts.length == 0) return res.send(mandatoryError);
                let result = await bannerProductCheck(bannerProducts);
                if(result.result == false) return res.send({apiStatus: false, statusCode : 200, result: {}, message: 'Product not found!'});
            }
            let crt = await db.Banner.create({name: name || '', image: image, bannerType: bannerType, offer: offer || 0, activeStatus : 1});
            if(crt) {
                let crtQuery = {
                    bannerId: crt.id,
                    activeStatus: 1
                }
                for (let d of bannerProducts) {
                    crtQuery.productId = d.id
                    await db.BannerProduct.create(crtQuery);
                }
            }
            return res.send({apiStatus: true, statusCode: 200, result: crt, message: 'Data Created'});

        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong)
        }
    },
    listBanner : async (req, res) => {
        try {
            let { page, limit } = req.query;
            page = parseInt(page) || 1;
            limit = parseInt(limit) || 10;
            let skip = (page - 1) * limit;
            let searchQuery = {
                where : {},
                limit: limit,
                offset : skip,
                include : [{model : db.BannerProduct, include : {model : db.Product}}]
            };
            let cc = await db.Banner.count({where : searchQuery.where});
            let finder = await db.Banner.findAll(searchQuery);
            let abs = cc / limit
            return res.send({apiStatus: true, statusCode: 200, message: 'Data Found!' , result: finder, count: Math.ceil(abs) })
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong)
        }
    },
    editBanner : async (req, res) => {
        try {
            const {id, user}  = req.token;
            const {bannerId, name, image, bannerType, offer, bannerProducts, activeStatus} = req.body;
            if(!bannerId || (!name && !image && !bannerType && !offer && !bannerProducts && !activeStatus)) return res.send(mandatoryError);
            let cker = await db.Banner.count({where : {id: bannerId}});
            if (cker == 0) return res.send({apiStatus: false, statusCode : 400, result: {}, message: 'Banner Not Found!'});
            let updateQuery = {};
            if (name && name != '') updateQuery.name = name;
            if (image && image != '') updateQuery.image = image;
            if (offer && offer != '') updateQuery.offer = offer;
            if (activeStatus && activeStatus != '') updateQuery.activeStatus = activeStatus;
            if (bannerType && bannerType != '') {
                if (bannerType != 1 || bannerType != 2) return res.send({apiStatus: false, statusCode : 400, message : 'Banner Type not found!', result : {}});
                updateQuery.bannerType = bannerType
            };
            let result = await bannerEditProductCheck(bannerProducts, bannerId);
            if(result.error == true) return res.send({apiStatus: false, statusCode : 200, result: {}, message: 'Product not found!'});
            let up = await db.Banner.update(updateQuery, {where: {id: bannerId}});
            let crtQuery = {
                bannerId: crt.id,
                activeStatus: 1
            }
            for (let d of bannerProducts) {
                crtQuery.productId = d.id
                await db.BannerProduct.create(crtQuery);
            }
            return res.send({apiStatus: true, statusCode: 200, result: {}, message: "Data Updated"});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong)
        }
    }
}