const {Op} = require('sequelize');
const db = require('../Model');
const { somethingWentWrong, mandatoryError, noAccess } = require("../Config");
const { productChecker } = require('../Service/Common');
const moment = require('moment');
const firebaseService = require('../Service/firebaseService');
module.exports = {
    createOrder : async (req, res) => {
        try {
            const {id, user} = req.token;
            const {durationId, voucher, total, products, address, contactId, offerAmount, deliveryType} = req.body;
            if (!durationId || !total || !products || !address || !contactId || !deliveryType) return res.send(mandatoryError);
            if (products.length == 0) return res.send(mandatoryError);
            let ckad = await db.UserAddressInfo.count({where : {id: address, user : id}});
            if (ckad == 0) return res.send({apiStatus : false, statusCode: 400, result : {} , message: 'Address is not valid!'});
            let ckct = await db.UserContactInfo.count({where : {id: contactId, user : id}});
            if (ckct == 0) return res.send({apiStatus : false, statusCode: 400, result : {} , message: 'Contact is not valid!'});
            var secret = Math.floor(1000 + Math.random() * 9000);
            if (deliveryType != 1 && deliveryType != 2) {
                return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'No Type Found!'});
            }
            let createQuery = {
                userId : id,
                secret : secret,
                address: address,
                deliveryType: deliveryType,
                contactId: contactId,
                status : 0,
                offerAmount: parseFloat(offerAmount) || 0,
                total: parseFloat(total)
            };
            let cket = await productChecker(products);
            if (cket.error == true) return res.send({apiStatus: false, statusCode: 426, message: cket.message, product: cket.product});
            if (voucher) {
                let cker =  await db.Offer.count({where : {id : voucher}});
                createQuery.voucher = voucher;
                if (cker == 0) return res.send({apiStatus: false, statusCode : 400, result : {}, message : 'Voucher Not found'})
            }
            let duc = await db.Duration.count({where : {id : durationId, activeStatus: 1}});
            if (duc == 0) return res.send({apiStatus: false , statusCode: 400, result : {}, message: 'Duration nor found!'});
            createQuery.durationId = durationId;
            let crt = await db.Order.create(createQuery);
            // val.productId, val.quantity, val.price, orderId
            for (let val of products) {
                await db.Product.update({stock : db.Sequelize.literal(`stock -${val.quantity}`)},{where : {id: val.productId}})
                await db.UserCart.destroy({where : {userId: id, productId : val.productId}});
                await db.OrderProducts.create({orderId: crt.id, productId: val.productId, quantity: val.quantity, price: val.price})
                let fd = await db.Product.findOne({where: {id: val.productId}});
                let findercart = await db.UserCart.count({where: {productId: val.productId, quantity : {[Op.gt] : fd.stock}}});
                if (findercart > 0) {
                    await db.UserCart.destroy({where: {productId: val.productId, quantity : {[Op.gt] : fd.stock}}});
                }
            }
            return res.send({apiStatus: true, statusCode : 200, result: crt, message: 'Order Created!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    listOrder : async (req, res) => {
        try {
            const {id, user} = req.token;
            let {page, limit, status, orderType} = req.query;
            page = parseInt(page) || 1;
            limit = parseInt(limit) || 10;
            let skip = (page - 1) * limit;
            let searchQuery = {
                where : {},
                limit : limit,
                offset : skip,
                order: [
                    ['createdAt', 'DESC']
                ],
            };
            if (user == 1) {
                searchQuery.where.userId = id;
            }
            if (status >= 0 && status <= 4) {
                searchQuery.where.status = status;
            }
            if(orderType && orderType == 'Pending') {
                searchQuery.where.status = 0
            }
            if(orderType && orderType == 'Way') {
                searchQuery.where.status = 1
            }
            if(orderType && orderType == 'Compleated') {
                searchQuery.where.status = 3
            }
            if(orderType && orderType == 'Cancel') {
                searchQuery.where.status = [ 2, 4 ]
            }
            let cc = await db.Order.count({where : searchQuery.where});
            searchQuery.include = [
                {
                    model: db.Rating,
                    attributes: ['id', 'rating', 'review', 'orderId']
                },
                {
                    model : db.OrderProducts, 
                    attributes: ['id', 'productId', 'quantity', 'price'], 
                    include : [
                        {
                            model: db.Product, 
                            attributes: ['id', 'name', 'price', 'image', 'units']
                        }
                    ]
                }, {
                    model : db.Duration
                }, {
                    model : db.UserAddressInfo, 
                    attributes: ['id', 'name', 'address', 'city', 'state', 'zip']
                }, {
                    model: db.UserContactInfo
                },
                {
                    model: db.User,
                    attributes: ['id', 'firstName', 'lastName']
                },
            ]
            let finder = await db.Order.findAll(searchQuery);
            let act = cc / limit;
            return res.send({apiStatus: true, statusCode: 200, result: finder, count : Math.ceil(act) ,message: 'Data Found'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    assignOrder : async (req, res) => {
        try {
            const { staffId, OrderId } = req.body;
            if(!staffId || !OrderId) return res.send(mandatoryError)
            let cket = await db.Order.count({where : {id: OrderId, status : [0,4]}});
            if (cket == 0) return res.send({apiStatus: false, statusCode : 400, message: 'Order Not Found'});
            let sidCheck = await db.Staff.count({where : {id : staffId}});
            if (sidCheck == 0) return res.send({apiStatus: false, statusCode : 400, message: 'Staff Not Found'});
            let ttc = await db.StaffOrder.count({where : {orderId: OrderId, status: [0,1]}});
            if (ttc > 0) return res.send({apiStatus: true, statusCode: 200, result: {}, message: 'Order User Already assigned to this to someone'});
            let crt = await db.StaffOrder.create({staffId: staffId, orderId: OrderId, status: 0});
            await db.Order.update({status: 1},{where : {id : OrderId}});
            firebaseService.addStaffNotification(staffId);
            await db.Notification.create({user: staffId, message: `A new Order#${OrderId} has been Assigned to you please respond!`, userType : 2});
            return res.send({apiStatus: true, statusCode: 200, message: 'Updated Sucessfully!', result: crt });
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    staffOrderConfiramtion : async (req, res) => {
        try {
            const {id, user} = req.token;
            if (user != 2) return res.send(noAccess);
            const {status, staffOrderId} = req.body;
            if (!status || !staffOrderId) return res.send(mandatoryError)
            let checker = await db.StaffOrder.findOne({where: {id: staffOrderId, status : 0}});
            if (!checker) return res.send({apiStatus: false, statusCode: 400, result : {}, message: 'Order Request Not Found!'});
            if (status != 1 && status != 2 ) return res.send({apiStatus: false, statusCode: 400, result : {}, message: 'Enter the valid Status!'});
            await db.StaffOrder.update({status: status},{where : {id: staffOrderId}})
            if (status == 2) {
                await db.Order.update({status: 4},{where : {id : checker.orderId}});
            } else {
                await db.Order.update({status: 1},{where : {id : checker.orderId}});
            }
            // FireBase Notification
            let ck = await db.Order.findOne({where: {id: checker.orderId}});
            firebaseService.addUserNotification(ck.userId);
            await db.Notification.create({user: ck.userId, message: `Your Order#${checker.orderId} has been confirmed at ${moment().format('LLL')}`, userType : 1});
            let tcount = await db.User.findOne({where: {id: ck.userId}});
            if (tcount && tcount.deviceToken != null) {
                firebaseService.pushNotification(`Your Order#${checker.orderId} has been confirmed at ${moment().format('LLL')}`, tcount.deviceToken)
            }
            // FireBase Notification
            return res.send({apiStatus: true, statusCode: 200, message: 'Updated Sucessfully!', result: {} });
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    listMyOrder : async(req, res) => {
        try {
            const {id, user} = req.token;
            if (user != 2) return res.send(noAccess);
            let {page, limit, status} = req.query;
            page = parseInt(page) || 1;
            limit = parseInt(limit) || 10;
            let skip = (page - 1) * limit;
            let searchQuery = {
                where : {staffId: id},
                limit : limit,
                offset : skip,
                order: [
                    ['createdAt', 'DESC']
                ],
            };
            if (status == 0 || status == 1 || status == 2 || status == 3) {
                searchQuery.where.status = parseInt(status);
            }
            let cc = await db.StaffOrder.count({where: searchQuery.where});
            searchQuery.include = [
                {
                    model: db.Order, 
                    include : [{
                        model : db.OrderProducts, 
                        attributes: ['id', 'productId', 'quantity', 'price'] , 
                        include : [{
                            model: db.Product, 
                            attributes: ['id', 'name', 'price', 'image', 'units']
                        }]
                    }, {
                        model : db.Duration
                    },{
                        model : db.UserAddressInfo, 
                        attributes: ['id', 'name', 'address', 'city', 'state', 'zip']
                    }, {
                        model : db.UserContactInfo
                    },{
                        model: db.User,
                        attributes: ['id', 'firstName', 'lastName']
                    }]
                }
            ]
            let finder = await db.StaffOrder.findAll(searchQuery);
            let abs = cc / limit;
            return res.send({apiStatus: true, statusCode : 200, result: finder, totalPage : Math.ceil(abs), totalCount: cc, message: 'Data Found!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    OrderRecieved : async(req, res) => {
        try {
            const {id, user} = req.token;
            if(user != 2) return res.send(noAccess);
            const {OrderId, secret} = req.body;
            if (!OrderId || !secret) return res.send(mandatoryError);
            let ck = await db.Order.findOne({where : {id: OrderId, status : 1}});
            if (!ck) return res.send({apiStatus: false, statusCode: 400, result : {}, message: 'Order Not Found!' });
            let ck2 = await db.StaffOrder.count({where : {staffId: id, orderId : OrderId, status : 1}});
            if (ck2 == 0) return res.send({apiStatus: false, statusCode : 400, result : {}, message: "You are not assigned to this Product!"});
            let ck3 = await db.Order.count({where : {id :OrderId, secret: secret}});
            if (ck3 == 0) return res.send({apiStatus: false, statusCode: 400, message: 'Otp Mis-Matches!'});
            if (ck.deliveryType == 2) {
                let ckr = await db.Staff.findOne({where : {id: id}, attributes: ['id', 'creditBalance']});
                let bal = parseFloat(ckr.creditBalance) + parseFloat(ck.total);
                await db.Staff.update({creditBalance: bal}, {where: {id: id}});
            }
            await db.Order.update({status : 3}, {where : {id: OrderId}});
            await db.StaffOrder.update({status: 3}, {where: {staffId: id, orderId : OrderId, status : 1}});
            firebaseService.addUserNotification(ck.userId);
            let tcount = await db.User.findOne({where: {id: ck.userId}});
            if (tcount && tcount.deviceToken != null) {
                firebaseService.pushNotification(`Your Order#${OrderId} has been delivered ${moment().format('LLL')}`, tcount.deviceToken)
            }
            await db.Notification.create({user: ck.userId, message: `Your Order#${OrderId} has been delivered ${moment().format('LLL')}`, userType : 1});
            return res.send({apiStatus: true, statusCode: 200, message: 'Data Updated', result : {}});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    }
}