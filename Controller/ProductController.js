let db = require('../Model');
const { somethingWentWrong, mandatoryError, noAccess } = require("../Config");
const { categoryChecker, validator } = require('../Service/Common');
const { Op } = require('sequelize');
let jwt = require('jsonwebtoken');
require('dotenv').config();
var Secret = process.env.JwtSecret || 'grocery';
const { Sequelize, sequelize } = require('../Model');

module.exports ={
    createProduct : async (req, res) => {
        try {
            const {id} = req.token;
            const {name, productImages, image, productCategory, stock, description, price, units, salesPrice} =  req.body;
            if (!name || !productImages || !image || !productCategory || !stock || !description || !price || !units || !salesPrice) return res.send(mandatoryError);
            if (productCategory.length <= 0) return res.send(mandatoryError);
            if (productImages.length <= 0) return res.send(mandatoryError);
            let checker = await categoryChecker(productCategory);
            if (checker == false) return res.send({apiStatus : false, statusCode : 400, result : {}, message : 'Category and sub category mismatches'});
            let imgChecker = await validator(productImages, 'image');
            if (imgChecker == false) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'Given image is invalid!'});
            let crt = await db.Product.create({name, image, stock, price, description, units, salesPrice});
            for (const item of productImages) {
                await db.ProductImage.create({product : crt.id, image : item.image});
            }
            for(const lest of productCategory) {
                if (lest.type.length > 0) {
                    for(let ap of lest.type){
                        await db.ProductCategory.create({product: crt.id, categoryId: lest.category, subcategoryId : lest.subcategory, subCategoryTypeId: ap.id});
                    }
                }
            }
            return res.send({apiStatus: true, statusCode : 200, result : crt, message: 'Product Created Sucessfully!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    listProduct : async (req, res) => {
        try {
            const {search, category, subcategory, sort} = req.query;
            let {page, limit} = req.query;
            page = parseInt(page) || 1;
            limit = parseInt(limit) || 10;
            let skip = (page - 1) * limit;
            let searchQuery = {
                where : {},
                offset : skip,
                limit: limit,
                order: [
                    ['createdAt', 'DESC']
                ],
            };
            if (sort && sort != '') {
                if (parseInt(sort) == 1) {
                    searchQuery.order = [
                        ['price', 'ASC']
                    ]
                } else {
                    searchQuery.order = [
                        ['price', 'DESC']
                    ]
                }
            }
            if (search) {
                searchQuery.where = {[Op.or] : [
                    {name : {
                        [Op.like] : `%${search}%`
                    }},
                    {description : {
                        [Op.like] : `%${search}%`
                    }}
                ]}

            }
            if (category && category != '') {
                let ckt  = await db.ProductCategory.count({
                    where : {
                        categoryId : category
                    },
                });
                if (ckt == 0) return res.send({apiStatus: true, statusCode: 200, result : [], message: 'No data found in this category!'})
                searchQuery.where.id = {
                    [Op.in] : Sequelize.literal(`(select product from productcategory WHERE categoryId = ${category})`)
                }
            }

            if (subcategory && subcategory != '') {
                let finder = await db.ProductCategory.count({
                    where : {
                        subcategoryId : subcategory
                    },
                });
                if (finder == 0) return res.send({apiStatus: true, statusCode: 200, result : [], message: 'No data found in this Sub category!'})
                searchQuery.where.id = {
                    [Op.in] : Sequelize.literal(`(select product from productcategory WHERE subcategoryId = ${subcategory})`)
                };
            }
            let cc = await db.Product.count(searchQuery);
            let count = cc / limit;
            console.log(count)
            searchQuery.attributes = ['id', 'name', 'image', 'price', 'stock', 'description', 'units', 'salesPrice', 'createdAt']
            searchQuery.include = [{model : db.ProductCategory,attributes: ['id'] , include : [{model: db.SubCategoryType, attributes: ['id', 'name']}]}, {model : db.ProductImage, attributes: ['id', 'image']}]
            let finder1 = await db.Product.findAll(searchQuery);
            return res.send({apiStatus: true, statusCode : 200, result : finder1, count: Math.ceil(count) , message: 'Data Found!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    listProductDetails : async(req, res) => {
        try {
            let user = 0
            if(req.headers && req.headers.authorization){
                var token = req.headers.authorization;
                var n = token.search("Bearer ");
                if (n < 0) return res.send(noAccess);
                token = token.replace('Bearer ', '');
                let verify = jwt.verify(token, Secret);
                if (!verify || !verify.id){
                    return res.send(noAccess);
                }
                user = verify.id;
            }
            const {id} = req.query;
            if (!id) return res.send(mandatoryError);
            let checker = await db.Product.count({where : {id: id}});
            if (checker == 0) return res.send({apiStatus: false, statusCode : 400, result: {}, message: 'Product Not Found'});
            let finder = await db.Product.findOne({
                where : {id: id},
                include : [{model : db.ProductCategory , attributes: ['id'] , include : [{model: db.SubCategoryType, attributes: ['id', 'name']}]}, {model : db.ProductImage, attributes: ['id', 'image']}]
            });
            let obj = {}
            if (finder) {
                obj = {
                    id: finder.id,
                    name: finder.name,
                    price: finder.price,
                    stock: finder.stock,
                    description: finder.description,
                    image: finder.image,
                    units: finder.units,
                    salesPrice: finder.salesPrice,
                    productimages : finder.productimages,
                    productcategories: finder.productcategories,
                    cartData: false,
                    cartValue: 0,
                }
                if (user != 0) {
                    let car = await db.UserCart.findOne({ where:  {userId: user, productId: finder.id}});
                    if (car) {
                        obj.cartData = true;
                        obj.cartValue = car.quantity;
                    }
                }
            }
            return res.send({apiStatus: true, statusCode: 200, result: obj, message: 'Data Found!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    listUserProduct : async (req, res) => {
        try {
            let userCrt = []
            let user = 0
            if(req.headers && req.headers.authorization){
                var token = req.headers.authorization;
                var n = token.search("Bearer ");
                if (n < 0) return res.send(noAccess);
                token = token.replace('Bearer ', '');
                let verify = jwt.verify(token, Secret);
                if (!verify || !verify.id){
                    return res.send(noAccess);
                }
                user = verify.id;
            }
            const {search, category, subcategory, sort, subcategoryId} = req.query;
            let {page, limit} = req.query;
            page = parseInt(page) || 1;
            limit = parseInt(limit) || 10;
            let skip = (page - 1) * limit;
            let searchQuery = {
                where : {},
                offset : skip,
                limit: limit,
                order: [
                    ['createdAt', 'DESC']
                ],

            };
            if (sort && sort != '') {
                if (parseInt(sort) == 1) {
                    searchQuery.order = [
                        ['price', 'ASC']
                    ]
                } else {
                    searchQuery.order = [
                        ['price', 'DESC']
                    ]
                }
            }
            if (search) {
                searchQuery.where = {[Op.or] : [
                    {name : {
                        [Op.like] : `%${search}%`
                    }},
                    {description : {
                        [Op.like] : `%${search}%`
                    }}
                ]}

            }
            if (category && category != '') {
                let ckt  = await db.ProductCategory.count({
                    where : {
                        categoryId : category
                    },
                });
                if (ckt == 0) return res.send({apiStatus: true, statusCode: 200, result : [], message: 'No data found in this category!'})
                searchQuery.where.id = {
                    [Op.in] : Sequelize.literal(`(select product from productcategory WHERE categoryId = ${category})`)
                }
            }

            if (subcategoryId && subcategoryId != '') {
                let finder = await db.ProductCategory.count({
                    where : {
                        subcategoryId : subcategoryId
                    },
                });
                if (finder == 0) return res.send({apiStatus: true, statusCode: 200, result : [], message: 'No data found in this Sub category!'})
                searchQuery.where.id = {
                    [Op.in] : Sequelize.literal(`(select product from productcategory WHERE subcategoryId = ${subcategoryId})`)
                };
            }

            if (subcategory && subcategory.length) {
                let finder = await db.ProductCategory.count({
                    where : {
                        subCategoryTypeId : subcategory
                    },
                });
                console.log('1111111111111111111111', subcategory);
                if (finder == 0) return res.send({apiStatus: true, statusCode: 200, result : [], message: 'No data found in this Sub category!'})
                let val = `(`;
                let st = true
                for(let map of subcategory) {
                    if (st == true) {
                        val = val + map;
                        st = false;
                    } else {
                        val = val + ', ' + map ;
                    }
                }
                val = val + ')'
                console.log(val);
                searchQuery.where.id = {
                    [Op.in] : Sequelize.literal(`(select product from productcategory WHERE subCategoryTypeId IN ${val})`)
                };
            }
            let cc = await db.Product.count({ where : searchQuery.where});
            let count = cc / limit;
            console.log(count)
            searchQuery.attributes = ['id', 'name', 'image', 'price', 'stock', 'units', 'salesPrice']
            let finder1 = await db.Product.findAll(searchQuery);
            let arr = [];
            for (const d of finder1) {
                let obj = {
                    id: d.id,
                    name: d.name,
                    image: d.image,
                    price: d.price,
                    stock: d.stock,
                    units: d.units,
                    salesPrice: d.salesPrice,
                    cartData: false,
                    cartValue: 0,
                };
                if (user != 0) {
                    let car = await db.UserCart.findOne({ where:  {userId: user, productId: d.id}});
                    if (car) {
                        obj.cartData = true;
                        obj.cartValue = car.quantity;
                    }
                }
                arr.push(obj);
            }
            return res.send({apiStatus: true, statusCode : 200, result : arr, count : Math.ceil(count) , message: 'Data Found!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    editProduct : async (req, res) => {
        try {
            const {id, user} = req.token;
            let {prodId, name, description, price, salesPrice, units, image, stock } = req.body;
            if (!prodId || (!name && !description && !price && !salesPrice && !units && !image && !stock)) return res.send(mandatoryError);
            let checker = await db.Product.count({id: prodId});
            if (checker == 0) return res.send({apiStatus: false, statusCode : 400, message: 'Product Not Found!', result: {}});
            let updateQuery = {}
            if (name && name != '') updateQuery.name = name;
            if (description && description != '') updateQuery.description = description;
            if (price && price != '') updateQuery.price = price;
            if (units && units != '') updateQuery.units = units;
            if (salesPrice && salesPrice != '') updateQuery.salesPrice = salesPrice;
            if (image && image != '') updateQuery.image = image;
            if (stock && stock != '') updateQuery.stock = stock;
            await db.Product.updateOne({id: prodId},updateQuery);
            return res.send({apiStatus: true, statusCode: 200, message: 'Product Updated', result: {}});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    }
}
