// StaffController
const { Op } = require("sequelize");
const { mandatoryError, somethingWentWrong, noAccess } = require('../Config');
const db = require('../Model');
let  PasswordService = require('../Service/PasswordService');
let jwt = require('../Service/jwt')
module.exports = {
    createStaff : async (req, res) => {
        try {
            let {firstName, lastName, email, password, gender ,phone, age, address, address1, city, state, zip} = req.body;
            if (!firstName || !gender || !email || !password || !phone || !age || !address || !city || !state || !zip) return res.send(mandatoryError);
            email = email.toLowerCase();
            let cker = await db.Staff.count({where : {email : email}});
            if (cker > 0) return res.send({apiStatus: false, statusCode : 400, message : "staff email Already exists!"});
            let hash = await PasswordService.generate(password);
            let createQuery = {
                firstName,
                lastName : lastName || '',
                email,
                password : hash,
                phone,
                gender : gender,
                age : parseInt(age),
                address,
                address1 : address1 || '',
                city,
                state,
                zip
            }
            let crt = await db.Staff.create(createQuery);
            return res.send({apiStatus: true, statusCode : 200, message: 'Data Created', result : crt});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    listStaff : async (req, res) => {
        try {
            const { id, user} = req.token;
            let {page, limit} = req.query;
            page = parseInt(page) || 1;
            limit = parseInt(limit) || 10;
            let skip = (page - 1) * limit;
            let cc = await db.Staff.count({where : {}});
            let act = cc / limit;
            let finder = await db.Staff.findAll({where: {}, limit: limit, offset : skip, order: [['createdAt', 'DESC']] })
            return res.send({apiStatus: true, statusCode: 200, message : 'Data Found!', result : finder, count : Math.ceil(act)});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    loginStaff : async ( req, res ) => {
        try {
            let { email, password, deviceToken } = req.body;
            if (!email || !password) return res.send(mandatoryError);
            email = email.toLowerCase()
            let emailCk = await db.Staff.findOne({where : {email : email}});
            if (!emailCk) return res.send({apiStatus: false, statusCode : 400, message: 'User Not Found!', result : {}});
            let passChecker = await PasswordService.verify(password, emailCk.password);
            console.log(passChecker);
            if(passChecker == false) return res.send({apiStatus: false, statusCode: 400, message: 'Password Missmatches!', result : {}});
            if(deviceToken && deviceToken != '') await db.Staff.update({deviceToken: deviceToken},{where : {id : emailCk.id}});
            let token = await jwt.generate({id: emailCk.id, user : 2});
            return res.send({apiStatus: true, statusCode: 200, message: 'Login sucess!', result :emailCk, token: token});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    staffDetails : async (req, res) => {
        try {
            const {id, user} = req.token;
            const {userId} = req.query;
            let searchQuery = {}
            if (user == 2) searchQuery.id = id;
            else {
                if (!userId) return res.send(mandatoryError)
                searchQuery.id = userId;
            }
            let finder = await db.Staff.findOne({where : searchQuery});
            return res.send({ apiStatus: true, statusCode: 200, result : finder, message: 'Data Found'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    editStaffData : async (req, res) => {
        try {
            const {id, user} = req.token;
            const {userId, firstName, lastName, gender ,phone, age, address, address1, city, state, zip} = req.body;
            if (firstName && !lastName && !gender && !phone && !age && !address && !address1 && !city && !state && !zip) return res.send(mandatoryError)
            let searchQuery = {};
            if (user == 1) return res.send(noAccess);
            if (user == 0) {
                if (!userId) return res.send(mandatoryError);
                searchQuery.id = userId;
            } else {
                searchQuery.id = id;
            }
            let cc = await db.Staff.count({where: searchQuery});
            if(cc == 0) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'User Not Found'});
            let updateQuery = {}
            if (firstName && firstName != '') updateQuery.firstName = firstName;
            if (lastName && lastName != '') updateQuery.lastName = lastName;
            if (gender && gender != '') updateQuery.gender = gender;
            if (phone && phone != '') updateQuery.phone = phone;
            if (age && age != '') updateQuery.age = age;
            if (address && address != '') updateQuery.address = address;
            if (address1 && address1 != '') updateQuery.address1 = address1;
            if (city && city != '') updateQuery.city = city;
            if (state && state != '') updateQuery.state = state;
            if (zip && zip != '') updateQuery.zip = zip;
            let up = await db.Staff.update(updateQuery, {where: searchQuery});
            return res.send({apiStatus: true, statusCode: 200, result :{}, message: 'Data updated'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    staffDashboard : async (req, res) => {
        try {
            const {id, user} = req.token;
            if (user != 2) return res.send(noAccess);
            let creditBalanceData = await db.Staff.findOne({where : {id: id}, attributes : ['id', 'creditBalance'] });
            if (!creditBalanceData) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'User not Found!'});
            let totalDelivery = await db.StaffOrder.count({where : {staffId: id}});
            let totalDeliverPending = await db.StaffOrder.count({where: {staffId: id, status: 1}});
            let totalDeliverDeclined = await db.StaffOrder.count({where: {staffId: id, status: 2}});
            let totalDeliverCompleated = await db.StaffOrder.count({where: {staffId: id, status: 3}});
            return res.send({apiStatus: true, statusCode: 200, result : {
                creditBalance: creditBalanceData.creditBalance,
                totalDelivery,
                totalDeliverPending,
                totalDeliverDeclined,
                totalDeliverCompleated
            }, message: 'Data Found!'})
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    }
}