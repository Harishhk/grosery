const { somethingWentWrong, mandatoryError, noAccess } = require("../Config");
const db = require("../Model");

module.exports = {
    createUserContact : async (req,res) => {
        try {
            const {id, user} = req.token;
            if (user == 0) return res.send(noAccess)
            const {name, number, primaryValue} = req.body;
            if(!number) return res.send(mandatoryError);
            let ck = await db.UserContactInfo.count({where : {user: id, activeStatus: 1}});
            console.log(id)
            console.log('1111111111111111111111',ck)
            if(ck >= 4) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'profile limit for contact is only 4 please try to edit!'})
            let createQuery = {
                number,
                user : id,
                activeStatus: 1
            };
            if(name && name != '') createQuery.name = name;
            else createQuery.name = '' ;
            if(primaryValue && primaryValue != '') {
                if (primaryValue == 1) {
                    await db.UserContactInfo.update({primaryValue : 0}, {where : {user : id}});
                }
                createQuery.primaryValue = primaryValue;
            }
            else createQuery.primaryValue = 0;
            let crt = await db.UserContactInfo.create(createQuery);
            return res.send({apiStatus : true, statusCode : 200, result: crt, message : 'Data Created!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    updateUserContact : async (req, res) => {
        try {
            const {id, user} = req.token;
            const {contactId, name, number, primaryValue} = req.body;
            if (user == 0) return res.send(noAccess);
            if (!contactId || (!name && !number && !primaryValue)) return res.send(mandatoryError);
            let checker = await  db.UserContactInfo.count({id: contactId});
            if (checker == 0) return res.send({apiStatus : false, statusCode: 400, result: {}, message: 'Contact info not found!'});
            let updateQurey = {};
            if(name && name != '') updateQurey.name = name;
            if(number && number != '') updateQurey.number = number;
            if(primaryValue && primaryValue != '') {
                if (primaryValue == 1) {
                    await db.UserContactInfo.update({primaryValue : 0}, {where : {user : id}});
                }
                updateQurey.primaryValue = primaryValue
            };
            let up = await db.UserContactInfo.update(updateQurey, {where: {id: contactId}});
            return res.send({apiStatus: true, statusCode: 200, result: up, message: 'Updated Sucessfully!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong)
        }
    },
    removeContact : async (req, res) => {
        try {
            const {id, user} = req.token;
            const {contactId} = req.body;
            if (user == 0) return res.send(noAccess);
            if (!contactId) return res.send(mandatoryError);
            let checker = await db.UserContactInfo.count({where : {id: contactId, user : id}});
            if (checker == 0) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'Data Not Found!'})
            let checker2 = await db.UserContactInfo.count({where : {id: contactId, freeze: 1}});
            if (checker2 == 1) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'Verified Contact can\'t be delete !'});
            let dest = await db.UserContactInfo.update({activeStatus: 0}, {where: {id: contactId}});
            return res.send({apiStatus: true, statusCode: 200, result: {}, message: 'Deleted Sucessfully!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong)
        }
    },
    listProfile : async( req, res) =>{
        try {
            const {id, user} = req.token;
            let searchQuery = {};
            let {userId} = req.query;
            if (user == 0) {
                if(!userId) return res.send(mandatoryError);
                searchQuery = {id : userId};
            } else {
                searchQuery = {id : id};
            }
            let cc = await db.User.count({where: searchQuery});
            if(cc == 0) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'User Not Found!'});
            let finder = await db.User.findOne({where: searchQuery});
            let addressinfos = await db.UserAddressInfo.findAll({where: {activeStatus: 1, user : searchQuery.id}});
            let constactInfos = await db.UserContactInfo.findAll({where: {activeStatus: 1, user : searchQuery.id}});
            let result = {
                id: finder.id,
                firstName: finder.firstName,
                lastName: finder.lastName,
                email: finder.email,
                password: finder.password,
                useraddressinfos: addressinfos || [],
                usercontactinfos : constactInfos || []
            }
            // console.log(finder);
            return res.send({apiStatus: true, statusCode: 200, result: result, message: 'Data Found!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    createUserAddressInfo : async (req, res) => {
        try {
            const {user, id} = req.token;
            if (user == 0) return res.send(noAccess);
            const {name, address, city, state, zip, primaryValue} = req.body;
            if(!name || !address || !city || !state || !zip) return res.send(mandatoryError);
            let ck = await db.UserAddressInfo.count({where : {user: id}});
            console.log(id)
            console.log('1111111111111111111111',ck)
            if(ck >= 4) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'profile limit for user address is only 4 please try to edit!'})
            let createQuery = {
                name,
                address,
                city,
                state,
                zip,
                user : id,
                activeStatus: 1
            }
            if(primaryValue && primaryValue != '') {
                if (primaryValue == 1) {
                    await db.UserAddressInfo.update({primaryValue : 0}, {where : {user : id}});
                }
                createQuery.primaryValue = primaryValue;
            } else createQuery.primaryValue = 0;
            let crt = await db.UserAddressInfo.create(createQuery);
            return res.send({apiStatus: true, statusCode: 200, result: crt, message: 'Data Found!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    updateUserAddressInfo : async (req, res) => {
        try {
            const {user, id} = req.token;
            if (user == 0) return res.send(noAccess)
            const {addressId, name, address, city, state, zip, primaryValue} = req.body;
            if(!addressId || (!name && !address && !city && !state && !zip && !primaryValue)) return res.send(mandatoryError);
            let checker = await db.UserAddressInfo.count({where: {id: addressId}});
            if (checker == 0) return res.send({apiStatus : false, statusCode: 400, result: {}, message: 'Address info not found!'});
            let updateQurey = {};
            if(name && name != '') updateQurey.name = name;
            if(address && address != '') updateQurey.address = address;
            if(city && city != '') updateQurey.city = city;
            if(state && state != '') updateQurey.state = state;
            if(zip && zip != '') updateQurey.zip = zip;
            if(primaryValue && primaryValue != '') {
                if (primaryValue == 1) {
                    await db.UserAddressInfo.update({primaryValue : 0}, {where : {user : id}});
                }
                updateQurey.primaryValue = primaryValue
            };
            let up = await db.UserAddressInfo.update(updateQurey, {where: {id: addressId}});
            return res.send({apiStatus: true, statusCode: 200, result: up, message: 'Data Updated!'})
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    removeAddress : async (req, res) => {
        try {
            const {id, user} = req.token;
            const {addressId} = req.body;
            if (user == 0) return res.send(noAccess);
            if (!addressId) return res.send(mandatoryError);
            let checker = await db.UserAddressInfo.count({where : {id: addressId, user : id}});
            if (checker == 0) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'Data Not Found!'})
            // let checker2 = await db.UserAddressInfo.count({where : {id: addressId, freeze: 1}});
            // if (checker2 == 1) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'Verified Contact can\'t be delete !'});
            let dest = await db.UserAddressInfo.update({activeStatus: 0},{where: {id: addressId}});
            return res.send({apiStatus: true, statusCode: 200, result: {}, message: 'Deleted Sucessfully!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong)
        }
    },
}
