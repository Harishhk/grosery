let db = require('../Model');
const { somethingWentWrong, mandatoryError, noAccess } = require("../Config");
const { Op } = require('sequelize');

module.exports = {
    addToCart : async (req, res) => {
        try {
            const {id, user} = req.token;
            let cker = await db.User.count({where: {id: id}});
            if (cker == 0) return res.send({apiStatus: false, statusCode: 400, result: {}, message : 'User Not Found!'});
            const {product, quantity} = req.body;
            if (!product || !quantity) return res.send(mandatoryError);
            let ck = await db.UserCart.findOne({ where:  {userId: id, productId : product}});
            let crt;
            let cker1 = await db.Product.findOne({
                where : {id: product}
            })
            if (!cker1) return res.send({apiStatus : true, statusCode: 400, message: 'Product Not Found!', result: {}});
            if (cker1.quantity < parseInt(quantity)) return res.send({apiStatus : true, statusCode: 400, message: 'Product quantity is exeded', result: {}});
            if (ck) {
                if (cker1.quantity < parseInt(quantity) ) return res.send({apiStatus : true, statusCode: 400, message: 'Product quantity is exeded', result: {}});
                crt = await db.UserCart.update({quantity: parseInt(quantity)},{
                    where: {
                        id: ck.id
                    }
                });
            } else {
                if (cker1.quantity < parseInt(quantity)) return res.send({apiStatus : true, statusCode: 400, message: 'Product quantity is exeded', result: {}});
                crt = await db.UserCart.create({userId: id, productId: product, quantity: quantity});
            }
            return res.send({apiStatus : true, statusCode: 200, result: crt, message: 'Data Updated!'})
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    listUserCart : async (req, res) => {
        try {
            const {id, user} = req.token;
            let cc = await db.UserCart.count({where : {userId: id}});
            let finder = await db.UserCart.findAll({where : {userId: id}, include : [{model: db.Product, attributes: ['id', 'name', 'price', 'units', 'image', 'stock']}]});
            return res.send({apiStatus: true, statusCode: 200, result: finder, count: cc ,message : "Data Found!"});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    removeCart : async(req, res) => {
        try {
            const {id, user} = req.token;
            const {product} = req.body;
            if (!product) return res.send(mandatoryError);
            let cker = await db.UserCart.count({ where:  {userId: id, productId : product}});
            if (cker == 0) return res.send({apiStatus: false, statusCode: 400, result: {}, message : "Alreaady Removed"})
            await db.UserCart.destroy({where : {userId: id, productId : product}});
            return res.send({apiStatus: true, statusCode: 200, result: {} ,message : "Data Deleted!"});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    editCart : async (req, res) => {
        try {
            const {id, user} = req.token;
            const {cartId, quantity} = req.body;
            if(!cartId || !quantity) return res.send(mandatoryError);
            let cker = await db.UserCart.findOne({where : {id: cartId}});
            if (!cher) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'cart details not found!'});
            let up = await db.UserCart.update({quantity: cker.quantity + parseInt(quantity)},{where: {id: cartId}});
            return res.send({apiStatus: true, statusCode: 200, message: 'Data Updated', result: up});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    }
}