const { Op } = require("sequelize");
const { mandatoryError, somethingWentWrong, noAccess } = require('../Config');
const db = require('../Model');
let  PasswordService = require('../Service/PasswordService');
let jwt = require('../Service/jwt');
const { rest } = require("lodash");
const UploadService = require("../Service/UploadService");
module.exports = {
    createAdmin : async (req, res) => {
        try {
            let {name, email, password} = req.body;
            if (!name || !email || !password) return res.send(mandatoryError);
            email = email.toLowerCase();
            let emailChecker = await db.Admin.count({where : {
                email: email
            }});
            if (emailChecker > 0) return res.send({apiStatus: false, statusCode : 400, result: {}, message : 'Email already exist!'});
            let hash = await PasswordService.generate(password);
            if (hash == false) return res.send(somethingWentWrong);
            let createQuery = {
                name,
                email,
                password: hash,
                deleteStatus: 0
            }
            let crt = await db.Admin.create(createQuery);
            return res.send({apiStatus: true, statusCode: 200, result: crt, message: 'Created Sucessfully!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    Adminlogin : async (req, res) => {
        try {
            let {email, password} = req.body;
            if (!email || !password) return res.send(mandatoryError)
            email = email.toLowerCase()
            let checker = await db.Admin.findOne({
                where : {
                    email: email
                }
            });
            if (!checker) return res.send({apiStatus: false, statusCode: 400, result: {}, message: "User not found" });
            let passChecker = await PasswordService.verify(password, checker.password);
            if(passChecker == false) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'Password Mismatches!'});
            let token = await jwt.generate({id: checker.id, user : 0});
            return res.send({apiStatus: true, statusCode: 200, result: checker, token, message: 'Data Found!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    changePassword: async(req, res) => {
        try {
            const {id, user} = req.token;
            const {oldPassword, newPassword} = req.body;
            if (!oldPassword || !newPassword) return res.send(mandatoryError);
            if (user == 0) {
                let checker = await db.Admin.findOne({
                    where : {
                        id: id
                    }
                });
                if(!checker) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'User not found!'});
                let comp = await PasswordService.verify(oldPassword, checker.password);
                if (comp == false) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'Password Mismatches!'});
                let hash = await PasswordService.generate(newPassword);
                if (hash == false) return res.send(somethingWentWrong);
                await db.Admin.update({password: hash}, {
                    where: {
                        id: id
                    }
                });
            } else if (user == 1) {
                let checker1 = await db.User.findOne({
                    where : {
                        id: id
                    }
                });
                if(!checker1) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'User not found!'});
                let comp1 = await PasswordService.verify(oldPassword, checker1.password);
                if (comp1 == false) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'Password Mismatches!'});
                let hash1 = await PasswordService.generate(newPassword);
                if (hash1 == false) return res.send(somethingWentWrong);
                await db.User.update({password: hash1}, {
                    where: {
                        id: id
                    }
                });
            } else if (user == 2) {
                let checker2 = await db.Staff.findOne({
                    where : {
                        id: id
                    }
                });
                if(!checker2) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'User not found!'});
                let comp2 = await PasswordService.verify(oldPassword, checker2.password);
                if (comp2 == false) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'Password Mismatches!'});
                let hash2 = await PasswordService.generate(newPassword);
                if (hash2 == false) return res.send(somethingWentWrong);
                await db.Staff.update({password: hash2}, {
                    where: {
                        id: id
                    }
                });
            } else {
                return res.send(noAccess)
            }
            return res.send({apiStatus: true, statusCode: 200, result: {}, message: 'Updated sucessfully!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    listUser : async (req, res) => {
        try {
            const {search} = req.query;
            let {page, limit} = req.query;
            page = parseInt(page) || 1;
            limit = parseInt(limit) || 10;
            let skip = (page - 1) * limit;
            let searchQuery = {
                where: {},
                offset : skip, 
                limit: limit
            };
            if(search && search != '') {
                searchQuery.where = {[Op.or] : [
                    {
                        firstName : {[Op.like] : `%${search}%`}
                    },
                    {
                        lastName : {[Op.like] : `%${search}%`}
                    },
                    {
                        email : {[Op.like] : `%${search}%`}
                    }]
                }
            }
            let cc = await db.User.count({where : searchQuery.where});
            let count  = cc / limit;
            let finder = await db.User.findAll(searchQuery);
            return res.send({apiStatus: true, statusCode: 200, count: Math.ceil(count), result: finder, message: 'Data Found!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    uploadImage : async (req, res) => {
        try {
            let uploadData = await UploadService.importImage(req, res);
        } catch (err) {
            console.log(err)
            res.json(somethingWentWrong);
        }
    },
    createDuration : async (req, res) => {
        try {
            const {id, user} = req.token;
            const {name, startTime, endTime} = req.body;
            let cker = await db.Duration.count();
            if (cker > 7) return res.send({apiStatus: false, statusCode: 400, message: 'Duration maximum limit is 7', result: {}});
            if(!name || !startTime) return res.send(mandatoryError);
            let crt = await db.Duration.create({name: name, startTime: startTime, endTime: endTime || '', activeStatus: 1});
            return res.send({apiStatus: true, statusCode: 200, message: 'created', result: crt});
        } catch (error) {
            console.log(error)
            res.json(somethingWentWrong);
        }
    },
    editDuration : async (req, res) => {
        try {
            const {id, user} = req.token;
            const {name, startTime, endTime, durationId} = req.body;
            if (!durationId || (!name && !startTime && !endTime)) return res.send(mandatoryError);
            let cker = await db.Duration.count({where: {id: durationId}});
            if (cker == 0) return res.send({apiStatus: false, statusCode: 400, message: 'Duration not Found', result: {}});
            let updateQuery = {};
            if (name && name != '') updateQuery.name = name;
            if (startTime && startTime != '') updateQuery.startTime = startTime;
            if (endTime && endTime != '') updateQuery.endTime = endTime;
            let up = await db.Duration.update(updateQuery, {where: {id: durationId}});
            return res.send({apiStatus: false, statusCode: 200, result: up, message: 'Data Updated!'});
        } catch (error) {
            console.log(error);
            res.json(somethingWentWrong);
        }
    },
    listDuration : async (req, res) => {
        try {
            let count = await db.Duration.count();
            let finder = await db.Duration.findAll();
            return res.send({apiStatus: true, statusCode: 200, message: 'Data Found!', result: finder, count});
        } catch (error) {
            console.log(error)
            res.json(somethingWentWrong)
        }
    },
}