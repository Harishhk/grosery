const db = require('../Model');
const {somethingWentWrong, mandatoryError} = require('../Config')
module.exports = {
    updatePrivacyTerms : async (req, res) => {
        try {
            const {text, id} = req.body;
            if (!text || !id) return res.send(mandatoryError);
            let finder = await db.PrivacyTerms.count({where : {id: id}});
            if (finder == 0) return res.send({apiStatus: false , statusCode: 400, result : {}, message: 'Data Not Found!'});
            let up = await db.PrivacyTerms.update({text: text}, {
                where : {
                    id: id
                }
            });
            return res.send({apiStatus: true, statusCode: 200, result: up, message: 'Updated Sucessfully!'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    listPrivacy : async (req, res) => {
        try {
            let finder = await db.PrivacyTerms.findOne({where : {status : 1}});
            if (!finder) return res.send({apiStatus : false, statusCode : 400, result : {}, message : 'Privacy policy not Found!'});
            return res.send({apiStatus: true, statusCode: 200, result: finder, message: 'Data Found'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    listTerms : async (req, res) => {
        try {
            let finder = await db.PrivacyTerms.findOne({where : {status : 2}});
            if (!finder) return res.send({apiStatus : false, statusCode : 400, result : {}, message : 'Terms and Condition not Found!'});
            return res.send({apiStatus: true, statusCode: 200, result: finder, message: 'Data Found'});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    }
}