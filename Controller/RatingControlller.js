let db = require('../Model');
const { somethingWentWrong, mandatoryError, noAccess } = require("../Config");
const { Op } = require('sequelize');
let jwt = require('jsonwebtoken');
const firebaseService = require('../Service/firebaseService');

module.exports = {
    addRatings : async (req, res) => {
        try {
            const {id, user} = req.token;
            const {orderId, rating, review} = req.body;
            if (!orderId || (!rating && !review)) return res.send(mandatoryError);
            let ck = await db.Order.count({where : {id: orderId, userId : id, status : 3}});
            let st = await db.StaffOrder.findOne({where : {orderId: orderId, status: 3}});
            if (ck == 0 || !st) return res.send({apiStatus: false, statusCode : 400, result : {}, message : 'Order Not Found or Not Compleated!'});
            let finder = await db.Rating.findOne({where : {orderId: orderId, userId: id}});
            if (!finder) {
                let createQuery = {
                    orderId: orderId,
                    staffId : st.staffId,
                    userId : id,
                    rating: parseFloat(rating) || 0,
                    review: review || ''
                }
                let userVal = await db.User.findOne({where : {id: id}});
                if (userVal) {
                    firebaseService.addStaffNotification(st.staffId);
                    await db.Notification.create({user: st.staffId, message: `${userVal.firstName} has given review for Order#${orderId}`, userType : 2});
                }
                let crt = await db.Rating.create(createQuery);
            } else {
                let updateQuery = {}
                if (rating || rating != 0) updateQuery.rating = parseFloat(rating);
                if (review || review != '') updateQuery.review = review;
                let up = await db.Rating.update(updateQuery, {where : {id: finder.id}});
            }
            return res.send({apiStatus: true, statusCode : 200, result: {}, message: "Updated sucessfully!" })
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    listRatings : async (req, res) => {
        try {
            const {id , user} = req.token;
            let {page, limit, orderId, userId, staffId} = req.query;
            page = parseInt(page) || 1;
            limit = parseInt(limit) || 10;
            let skip = (page - 1) * limit;
            let searchQuery = {
                where : {},
                offset : skip, 
                limit: limit,
                order: [
                    ['createdAt', 'DESC']
                ],
                include : [{model : db.Order, attributes: ['id', 'total']}, {model : db.User, attributes: ['id', 'firstName', 'lastName', 'email']}, {model : db.Staff, attributes: ['id', 'firstName', 'lastName', 'email', 'gender']} ]
            }
            if (user == 0) {
                if (userId && userId != '') searchQuery.where.userId = userId;
                if (staffId && staffId != '') searchQuery.where.staffId = staffId;
                if (orderId && orderId != '') searchQuery.where.orderId = orderId;
            } else if ( user == 1) {
                searchQuery.where.userId = id;
            } else {
                searchQuery.where.staffId = id;
            }
            let cc = await db.Rating.count({where : searchQuery.where});
            let finder = await db.Rating.findAll(searchQuery);
            let abs = cc / limit;
            return res.send({apiStatus: true, statusCode : 200, result: finder, count: Math.ceil(abs) ,message: 'Data Found!'});   
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    },
    listOrderDetails : async (req, res) => {
        try {
            let {id, user} = req.token;
            let {orderId} = req.query;
            if (!orderId) return res.send(mandatoryError);
            let ck = await db.Order.count({where: {id: orderId}});
            if (ck == 0) return res.send({apiStatus: false, statusCode: 400, result: {}, message: 'Order Not Found!'})
            let searchQuery = {
                where : {id: orderId},
            };
            searchQuery.include = [
                {
                    model: db.Rating,
                    attributes: ['id', 'rating', 'review', 'orderId']
                },
                {
                    model : db.OrderProducts, 
                    attributes: ['id', 'productId', 'quantity', 'price'], 
                    include : [
                        {
                            model: db.Product, 
                            attributes: ['id', 'name', 'price', 'image', 'units']
                        }
                    ]
                }, {
                    model : db.Duration
                }, {
                    model : db.UserAddressInfo, 
                    attributes: ['id', 'name', 'address', 'city', 'state', 'zip']
                }, {
                    model: db.UserContactInfo
                },
                {
                    model: db.User,
                    attributes: ['id', 'firstName', 'lastName']
                },
            ]
            let finder = await db.Order.findOne(searchQuery);
            return res.send({apiStatus: true, statusCode: 300, message: 'Data Found!', result: finder});
        } catch (error) {
            console.log(error);
            return res.send(somethingWentWrong);
        }
    }
}